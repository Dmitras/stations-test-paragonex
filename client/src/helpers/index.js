
export const sortStationsByName = (stations) => {
    function compare(a, b) {
        return a.name > b.name ? 1 : -1;
    }

    return stations.sort(compare)
}

export const sortStationsByHighestTemperature = (stations, reverse) => {
    const arrayMax = (arr) => {
        return arr.reduce(function (p, v) {
            return (p > v ? p : v);
        });
    }

    function keySort(arr, prop, reverse) {
        let sortOrder = 1;
        if (reverse) sortOrder = -1;
        return arr.sort(function (a, b) {
            let x = arrayMax(a[prop]);
            let y = arrayMax(b[prop]);

            return sortOrder * ( (x < y) ? -1 : ((x > y) ? 1 : 0) );
        });
    }

    return keySort(stations, 'points', reverse)
}