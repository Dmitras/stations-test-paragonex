import React from 'react';
import {
    FILTER_BY_NAME,
    SORT_BY_NAME,
    SORT_BY_MAX_TEMP,
    SORT_BY_MIN_TEMP,
    SET_DATA_LOADING,
    GET_INITIAL_DATA,
    SET_DEFAULTS
} from "./types/types";
import axios from "axios";


export const getInitialData = () => {
    let isLoading = true
    return async (dispatch) => {
        dispatch(setDataLoading(isLoading))

        try {
            const response = await axios.get('/api/v1/init')
            const data = response.data

            dispatch(getDataSuccess(data))
            isLoading = false
            dispatch(setDataLoading(isLoading))
        } catch (e) {
            dispatch(setDataLoading(isLoading))
            console.log(e)
        } finally {
            dispatch(setDataLoading(false))
        }
    }
}

export const setDataLoading = (data) => {
    return {
        type: SET_DATA_LOADING,
        payload: data
    }
}

export const getDataSuccess = (data) => {
    return {
        type: GET_INITIAL_DATA,
        payload: data
    }
}


export const filterByName = (name) => {
    return (dispatch) => {
        dispatch({
            type: FILTER_BY_NAME,
            payload: {name: name, aggregateBy: 'name'}
        })
    }
}

export const sortByName = (sortBy) => {
    return (dispatch) => {
        dispatch({
            type: SORT_BY_NAME,
            payload: {name: null, aggregateBy: sortBy}
        })
    }
}


export const sortByHighestTemp = (sortBy) => {
    return (dispatch) => {
        dispatch({
            type: SORT_BY_MAX_TEMP,
            payload: {name: null, aggregateBy: sortBy}
        })
    }
}

export const sortByLowestTemp = (sortBy) => {
    return (dispatch) => {
        dispatch({
            type: SORT_BY_MIN_TEMP,
            payload: {name: null, aggregateBy: sortBy}
        })
    }
}

export const setDefaults = () => {
    return (dispatch) => {
        dispatch({
            type: SET_DEFAULTS,
            payload: {aggregatedStations: null, aggregateBy: ''}
        })
    }
}
