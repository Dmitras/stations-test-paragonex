import {
    GET_INITIAL_DATA,
    SET_DATA_LOADING,
    FILTER_BY_NAME,
    SORT_BY_NAME,
    SORT_BY_MAX_TEMP,
    SORT_BY_MIN_TEMP,
    SET_DEFAULTS
} from '../actions/types/types';
import {sortStationsByName, sortStationsByHighestTemperature} from '../../helpers/index';

const initialState = {
    clientKey: '',
    items: [],
    aggregateBy: '',
    aggregatedStations: []
};


const setAggregatedStations = (state, name, sortBy) => {
    console.log(name, sortBy, "LOG REDUCER");

    let arr = []

    if (name) {
        refillAggregatedStations()
        return arr.filter(station => station.name === name)

    } else if (sortBy === 'nameSort') {
        refillAggregatedStations()
        return sortStationsByName(arr)

    } else if (sortBy === 'maxTempSort') {
        refillAggregatedStations()
        return sortStationsByHighestTemperature(arr, 'reverse')

    } else if (sortBy === 'minTempSort') {
        refillAggregatedStations()
        return sortStationsByHighestTemperature(arr)

    } else if (!state.aggregatedStations) {
        refillAggregatedStations()
    }

    function refillAggregatedStations() {
        const stations = state.stations

        for (let station in stations) {
            arr.push(Object.assign({}, {
                name: station,
                enabled: stations[station].enabled,
                points: stations[station].points
            }))
        }
    }

    return arr
}


export default function (state = initialState, {type, payload}) {
    switch (type) {
        case GET_INITIAL_DATA:
            return {
                ...state,
                stations: payload.stations,
                clientKey: payload.clientKey,
                time: payload.time
            }

        case SET_DEFAULTS:
            return {
                ...state,
                aggregatedStations: payload.aggregatedStations,
                aggregateBy: payload.aggregateBy
            }

        case SET_DATA_LOADING:
            return {
                ...state,
                isLoading: payload
            }

        case FILTER_BY_NAME:
            return {
                ...state,
                aggregatedStations: setAggregatedStations(state, payload.name, null),
                aggregateBy: payload.aggregateBy
            }

        case SORT_BY_NAME:
            return {
                ...state,
                aggregatedStations: setAggregatedStations(state, payload.name, payload.aggregateBy),
                aggregateBy: payload.aggregateBy
            }

        case SORT_BY_MAX_TEMP:
            return {
                ...state,
                aggregatedStations: setAggregatedStations(state, payload.name, payload.aggregateBy),
                aggregateBy: payload.aggregateBy
            }

        case SORT_BY_MIN_TEMP:
            return {
                ...state,
                aggregatedStations: setAggregatedStations(state, payload.name, payload.aggregateBy),
                aggregateBy: payload.aggregateBy
            }

        default:
            return {
                state
            }
    }
}
