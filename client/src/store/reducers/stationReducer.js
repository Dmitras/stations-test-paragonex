import { FILTER_BY_NAME, SORT_BY_NAME, SORT_BY_MAX_TEMP, SORT_BY_MIN_TEMP, } from '../actions/types/types';

const initialState = {
    items: []
};



export default function (state = initialState, {type, payload}) {
    switch (type) {
        case FILTER_BY_NAME:
            // console.log(payload, 'FILTER_BY_NAME');
            return {
                ...state,
                items: payload
            }

        case SORT_BY_NAME:
            // console.log(payload, 'SET_DATA_LOADING');
            return {
                ...state,
                isLoading: payload
            }

        default:
            return {
                state
            }
    }
}
