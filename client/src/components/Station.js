import React, {useState, useEffect} from 'react';
import Chart from "./Chart";
import {Row} from "reactstrap";
import {Badge, Button} from "reactstrap";
import Request from "axios-request-handler";
import axios from "axios";

const Station = (props) => {
    const {clientKey, name, points} = props
    const [state, setState] = useState(null)
    const [data, setUpdateData] = useState(null)
    const [delta, setDelta] = useState([])

    useEffect(() => {
        setState(normalizedChartData(points))
    }, [])

    useEffect(() => {
        setTimeout(() => {
            getData()
        }, 3000)
    }, [])

    useEffect(() => {
        if (data) {
            setDelta(data.delta)
            setState(normalizedChartData(delta))
        }
    }, [data])


    const getData = async () => {
        if (props) {
            let {time} = props
            if (data) {
                time = data.time
            }

            const request = new Request(`/api/v1/client/${clientKey}/delta/${name}/since/${time}`)

            await request.poll(1000).get((res) => {
                console.log(res);
                setUpdateData(res.data);
            })
        }
    }

    // const getData = async () => {
    //     if (props) {
    //
    //         let {clientKey, time} = props
    // if (data) {
    //     time = data.time
    // }
    //
    //         // try {
    //         //     const response = await axios.get(`/api/v1/client/${clientKey}/delta/${name}/since/${time}`)
    //         //         .then(res => res.data)
    //         //     setUpdateData(response)
    //         //
    //         // } catch (error) {
    //         //     console.log(error);
    //         // }
    //         // .catch(function (error) {
    //         //     if (error.response) {
    //         //         // Request made and server responded
    //         //         console.log(error.response.data);
    //         //         console.log(error.response.status);
    //         //         console.log(error.response.headers);
    //         //     } else if (error.request) {
    //         //         // The request was made but no response was received
    //         //         console.log(error.request);
    //         //     } else {
    //         //         // Something happened in setting up the request that triggered an Error
    //         //         console.log('Error', error.message);
    //         //     }
    //         //
    //         // });
    //
    //
    //         const request = new Request(`/api/v1/client/${clientKey}/delta/${name}/since/${time}`, {
    //             errorHandler: (error, method) => {
    //                 console.log('11111')
    //             }
    //         })
    //
    //         try {
    //             const response =  await request.poll(4500).get((res) => {
    //                 // console.log(res.data);
    //                 return res
    //             })
    //
    //             setUpdateData(response);
    //
    //         } catch (error) {
    //             console.log(error)
    //         }
    //     }
    // }


    const normalizedChartData = (data) => {
        let chartData = []

        data.map((item, i) => {
            chartData.push(Object.assign({}, {temperature: item, index: i}))
        })

        return chartData
    }

    const renderEnabledBadge = () => {
        if (data) {
            if (data.enabled === true) {
                return <Badge color="success">Enabled</Badge>
            } else {
                return <Badge color="danger">Disabled</Badge>
            }
        }
    }

    return (
        <>
            <h2>
                Hello, I'm station {name}&nbsp;
                {renderEnabledBadge()}
            </h2>
            <Row>
                <Chart data={state}/>
            </Row>
        </>

    );
};

export default Station;