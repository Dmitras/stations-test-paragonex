import React from 'react';
import {Legend, Line, LineChart ,AreaChart, XAxis, YAxis, CartesianGrid, Tooltip, Area, ResponsiveContainer } from "recharts";

const Chart = (props) => {

    const { data } = props

    return (
        <ResponsiveContainer width='100%' height={500}>
            <LineChart data={data}
                       margin={{top: 10, right: 0, left: -10, bottom: 0}}>
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis />
                <YAxis />
                <Tooltip />
                <Legend />
                <Line type="monotone" dataKey="temperature" stroke="#8884d8" />
            </LineChart>
        </ResponsiveContainer >
    );
};

export default Chart;