import React from 'react';
import Wrapper from "../hoc/Wrapper";

function Layout(props) {
    return (
        <Wrapper>
            <main>
                {props.children}
            </main>
        </Wrapper>
    );
}

export default Layout;