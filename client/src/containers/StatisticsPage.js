import React, {Component, Suspense} from 'react';
import {connect} from "react-redux";
import {
    getInitialData,
    filterByName,
    setDefaults,
    sortByName,
    sortByHighestTemp,
    sortByLowestTemp
} from '../store/actions/index'
import Station from "../components/Station";
import Wrapper from "../hoc/Wrapper";
import {
    Container,
    Button,
    ButtonGroup,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Navbar,
    Nav,
    Input,
    Label,
} from "reactstrap";

class StatisticsPage extends Component {
    filterByNameHandler = (name) => {
        const {filterByName} = this.props
        filterByName(name)
    }

    defaultsHandler = () => {
        const {setDefaults} = this.props
        setDefaults()
    }

    sortByNameHandler = (name) => {
        const {sortByName} = this.props
        sortByName(name)
    }

    sortByMaxTempHandler = (name) => {
        const {sortByName} = this.props
        sortByName(name)
    }

    setOnlineHandler = () => {

    }


    getStations = () => {
        if (this.props) {
            const { stations } = this.props

            let arr = []

            for (let station in stations) {
                arr.push(Object.assign({}, {
                    name: station,
                    enabled: stations[station].enabled,
                    points: stations[station].points
                }))
            }

            return arr
        }
    }

    getAggregatedStations = () => {
        if (this.props.aggregatedStations) {
            return this.props.aggregatedStations
        }
    }

    renderStations = () => {
        let stations;
        if (this.props.aggregateBy && this.props.aggregateBy.length) {
            stations = this.getAggregatedStations()
        } else {
            stations = this.getStations()
        }

        return stations ? stations.map(station =>
            <Station key={station.name} time={this.props.time} clientKey={this.props.clientKey} name={station.name} enabled={station.enabled} points={station.points}/>
        ) : ''
    }

    renderSelect = () => {
        const stationsNames = () => {
            if (this.props.stations) {
                const { stations } = this.props

                return Object.keys(stations).map(name => {
                    if (this.props.aggregatedStations === null) {
                        return (
                            <>
                                <option>{name}</option>
                                <option defaultValue="" selected disabled hidden>Choose station</option>
                            </>
                        )
                    } else {
                        return (
                            <>
                                <option>{name}</option>
                            </>
                        )
                    }
                })
            }
        }

        return (
            <>
                <Label for="exampleSelect">Filter by Name</Label>
                <Input onChange={(e) => this.filterByNameHandler(e.target.value)} type="select" name="select"
                       id="exampleSelect">
                    {stationsNames()}
                    <option defaultValue="" selected disabled hidden>Choose station</option>
                </Input>
            </>
        )
    }

    componentDidMount() {
        this.props.getInitialData()
        // this.interval = setInterval(() => {
        //     this.getData();
        // }, 4000);

    }

    componentDidUpdate() {

    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {

        return (
            <Wrapper>
                <Container>
                    <Navbar>
                        <Nav>
                            {this.renderSelect()}
                        </Nav>
                        <Nav>
                            <InputGroup>
                                <InputGroupAddon addonType="prepend">
                                    <InputGroupText>Sort by</InputGroupText>
                                </InputGroupAddon>
                                <ButtonGroup>
                                    <Button onClick={this.defaultsHandler}>Default</Button>
                                    <Button onClick={() => this.sortByNameHandler('nameSort')}>Name</Button>
                                    <Button onClick={() => this.sortByNameHandler('maxTempSort')}>
                                        Max temperature</Button>
                                    <Button onClick={() => this.sortByNameHandler('minTempSort')}>
                                        Min temperature</Button>
                                </ButtonGroup>
                            </InputGroup>
                        </Nav>

                    </Navbar>

                    <div>
                        {this.renderStations()}
                    </div>
                </Container>
            </Wrapper>
        );
    }
}

const mapStateToProps = ({items}) => ({
    stations: items.stations,
    clientKey: items.clientKey,
    time: items.time,
    data: items.items,
    isLoading: items.isLoading,
    aggregatedStations: items.aggregatedStations,
    aggregateBy: items.aggregateBy
})

const mapDispatchToProps = dispatch => {
    return {
        filterByName: (name) => dispatch(filterByName(name)),
        getInitialData: () => dispatch(getInitialData()),
        setDefaults: () => dispatch(setDefaults()),
        sortByName: (sortBy) => dispatch(sortByName(sortBy)),
        sortByHighestTemp: (sortBy) => dispatch(sortByHighestTemp(sortBy)),
        sortByLowestTemp: (sortBy) => dispatch(sortByLowestTemp(sortBy)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StatisticsPage);